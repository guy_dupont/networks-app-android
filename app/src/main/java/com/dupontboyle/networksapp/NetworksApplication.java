package com.dupontboyle.networksapp;

import android.app.Application;
import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;

import timber.log.Timber;

public class NetworksApplication extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        Timber.plant(new Timber.DebugTree());
        Timber.d(FirebaseInstanceId.getInstance().getToken());
    }
}
