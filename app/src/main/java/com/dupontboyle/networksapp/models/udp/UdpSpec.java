package com.dupontboyle.networksapp.models.udp;

import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.google.gson.JsonObject;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;

public enum UdpSpec {
    CLIENT_PROGRESS_UPDATE {
        @Override
        void parseFromUdpInternal(byte[] rawBytes, CloudMessageInterface cloudMessageInterface) {
            int jobIdLength = rawBytes[1];
            String jobId = new String(rawBytes, 2, jobIdLength);
            int firstCountOffset = 2 + jobIdLength;
            int secondCountOffset = 8 + firstCountOffset;
            ByteBuffer byteBuffer = ByteBuffer.wrap(rawBytes);
            long processed = byteBuffer.getLong(firstCountOffset);
            long total = byteBuffer.getLong(secondCountOffset);
            cloudMessageInterface.onJobProgressUpdate(jobId, processed, total);
        }

        @Override
        void parseFromFcmInternal(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface) {
            String jobId = jsonObject.get("job_id").getAsString();
            long processed = jsonObject.get("processed").getAsLong();
            long total = jsonObject.get("total").getAsLong();
            cloudMessageInterface.onJobProgressUpdate(jobId, processed, total);
        }
    },
    CLIENT_JOB_DONE {
        @Override
        void parseFromUdpInternal(byte[] rawBytes, CloudMessageInterface cloudMessageInterface) {
            int jobIdLength = rawBytes[1];
            cloudMessageInterface.onJobFinished(new String(rawBytes, 2, jobIdLength));
        }

        @Override
        void parseFromFcmInternal(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface) {
            cloudMessageInterface.onJobFinished(jsonObject.get("job_id").getAsString());
        }
    },
    GEAR_SHOULD_QUERY {
        @Override
        void parseFromUdpInternal(byte[] rawBytes, CloudMessageInterface cloudMessageInterface) {
            cloudMessageInterface.onGearPoke();
        }

        @Override
        void parseFromFcmInternal(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface) {
            cloudMessageInterface.onGearPoke();
        }
    }, CLIENT_START_STREAM {
        @Override
        void parseFromUdpInternal(byte[] rawBytes, CloudMessageInterface cloudMessageInterface) {
            int ipAddressLength = rawBytes[1];
            cloudMessageInterface.onStreamAddress(new String(rawBytes, 2, ipAddressLength));
        }

        @Override
        void parseFromFcmInternal(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface) {
            cloudMessageInterface.onStreamAddress(jsonObject.get("address").getAsString());
        }
    };

    abstract void parseFromUdpInternal(byte[] rawBytes, CloudMessageInterface cloudMessageInterface);
    abstract void parseFromFcmInternal(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface);

    public static void parseFromUdp(DatagramPacket packet, CloudMessageInterface cloudMessageInterface){
        if (packet != null && packet.getLength() > 0) {
            byte[] rawBytes = packet.getData();
            int specCode = rawBytes[0];
            if (specCode < 0 || specCode >= UdpSpec.values().length - 1) {
                cloudMessageInterface.onParseError(rawBytes);
                return;
            }
            UdpSpec spec = UdpSpec.values()[specCode];
            spec.parseFromUdpInternal(rawBytes, cloudMessageInterface);

         } else {
            cloudMessageInterface.onParseError(null);
        }
    }

    public static void parseFromFcm(JsonObject jsonObject, CloudMessageInterface cloudMessageInterface) {
        try {
            int specCode = jsonObject.get("type").getAsInt();
            UdpSpec spec = UdpSpec.values()[specCode];
            spec.parseFromFcmInternal(jsonObject, cloudMessageInterface);
        } catch (Exception e) {
            cloudMessageInterface.onParseError(null);
        }
    }
}
