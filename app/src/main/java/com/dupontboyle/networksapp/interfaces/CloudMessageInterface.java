package com.dupontboyle.networksapp.interfaces;

public interface CloudMessageInterface {
    void onJobProgressUpdate(String jobId, long processed, long totalLength);
    void onStreamAddress(String streamAddress);
    void onJobFinished(String jobId);
    void onGearPoke();
    void onParseError(byte[] bytes);
}
