package com.dupontboyle.networksapp.interfaces;

public interface CloudMessageReceiver {
    void register(final CloudMessageInterface cloudMessageInterface);
    void unregister(CloudMessageInterface cloudMessageInterface);
}
