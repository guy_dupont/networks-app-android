package com.dupontboyle.networksapp.screens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.models.GearDef;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

class GearDefsAdapter extends RecyclerView.Adapter<GearDefsAdapter.ViewHolder>{
    private final PublishSubject<GearDef> onClickSubject = PublishSubject.create();

    private List<GearDef> gearDefs;

    GearDefsAdapter(List<GearDef> gearDefs) {
        this.gearDefs = gearDefs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gear_def_cell, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.configure(gearDefs.get(position));
    }

    @Override
    public int getItemCount() {
        return gearDefs.size();
    }

    Observable<GearDef> getItemClicks() {
        return onClickSubject.asObservable();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.make_text)
        TextView makeTextView;
        @BindView(R.id.model_text)
        TextView modelTextView;
        @BindView(R.id.type_text)
        TextView typeTextView;

        ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        void configure(GearDef gearDef) {
            makeTextView.setText(gearDef.getMake());
            modelTextView.setText(gearDef.getModel());
            typeTextView.setText(gearDef.getType().value());
        }

        @Override
        public void onClick(View v) {
            onClickSubject.onNext(gearDefs.get(getAdapterPosition()));
        }
    }
}
