package com.dupontboyle.networksapp.screens;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.dupontboyle.networksapp.interfaces.CloudMessageReceiver;
import com.dupontboyle.networksapp.messaging.FcmReceiver;
import com.dupontboyle.networksapp.models.Job;
import com.dupontboyle.networksapp.net.JobsResponseBody;
import com.dupontboyle.networksapp.net.RetrofitClient;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Completable;
import rx.CompletableEmitter;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class JobsActivity extends AppCompatActivity implements CloudMessageInterface {
    @BindView(R.id.recyclerView)
    RecyclerView jobsRecyclerView;

    private List<Job> jobs;
    private JobsAdapter adapter;
    private CompositeSubscription subscriptions = new CompositeSubscription();
    private CloudMessageReceiver cloudMessageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);

        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        jobsRecyclerView.setLayoutManager(layoutManager);

        jobs = new ArrayList<>();
        adapter = new JobsAdapter(jobs);
        jobsRecyclerView.setAdapter(adapter);
        cloudMessageReceiver = new FcmReceiver(this);

        subscriptions.add(adapter.getItemClicks()
                                  .subscribe(new Action1<Job>() {
                                      @Override
                                      public void call(Job job) {
                                          downloadFile(job.getId(), job.getName());
                                      }
                                  }));

    }

    @Override
    protected void onStart() {
        super.onStart();
        cloudMessageReceiver.register(this);
        loadJobs();
    }

    @Override
    protected void onStop() {
        super.onStop();
        cloudMessageReceiver.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        subscriptions.clear();
    }

    private void loadJobs() {
        String username = SharedPreferencesManager.getInstance(this).getUsername();
        subscriptions.add(RetrofitClient.getBackendApi(this).getJobs(username)
                                  .subscribeOn(Schedulers.io())
                                  .observeOn(AndroidSchedulers.mainThread())
                                  .subscribe(new Action1<JobsResponseBody>() {
                                      @Override
                                      public void call(JobsResponseBody jobsResponseBody) {
                                          updateJobs(jobsResponseBody.getJobs());
                                      }
                                  }, new Action1<Throwable>() {
                                      @Override
                                      public void call(Throwable throwable) {
                                          Timber.e(throwable);
                                      }
                                  }));
    }

    private void updateJobs(List<Job> jobs) {
        this.jobs.clear();
        this.jobs.addAll(jobs);
        adapter.notifyDataSetChanged();
    }

    private void downloadFile(final String jobId, final String jobName) {
        // note, file write not done on the main thread
        subscriptions.add(RetrofitClient.getBackendApi(this).downloadFile(jobId)
                                  .subscribeOn(Schedulers.io())
                                  .toSingle()
                                  .flatMapCompletable(new Func1<Response<ResponseBody>, Completable>() {
                                      @Override
                                      public Completable call(Response<ResponseBody> responseBodyResponse) {
                                          return writeFileToStorage(responseBodyResponse.body(), jobName);
                                      }
                                  })
                                  .observeOn(AndroidSchedulers.mainThread())
                                  .subscribe(new Action0() {
                                      @Override
                                      public void call() {
                                          Toast.makeText(JobsActivity.this, R.string.download_complete, Toast.LENGTH_SHORT)
                                                  .show();
                                      }
                                  }, new Action1<Throwable>() {
                                      @Override
                                      public void call(Throwable throwable) {
                                          Toast.makeText(JobsActivity.this, R.string.error_downloading, Toast.LENGTH_SHORT)
                                                  .show();
                                          Timber.e(throwable);
                                      }
                                  }));
    }

    private Completable writeFileToStorage(final ResponseBody body, final String jobName) {
        return Completable.fromEmitter(new Action1<CompletableEmitter>() {
            @Override
            public void call(CompletableEmitter completableEmitter) {
                try {
                    String filename =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
                                    .getAbsolutePath() + "/" +
                                    jobName;
                    InputStream inputStream = body.byteStream();
                    FileOutputStream fileOutputStream = new FileOutputStream(filename);
                    byte[] buffer = new byte[1024];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        fileOutputStream.write(buffer, 0, bytesRead);
                    }
                    inputStream.close();
                    fileOutputStream.close();
                    completableEmitter.onCompleted();
                } catch (IOException e) {
                    completableEmitter.onError(e);
                }
            }
        });

    }

    /*
    Following methods called by RxUdpReceiver on receipt of UDP message
     */

    @Override
    public void onJobProgressUpdate(String jobId, long processed, long totalLength) {
        // TODO: 11/20/16
    }

    @Override
    public void onStreamAddress(String streamAddress) {

    }

    @Override
    public void onJobFinished(String jobId) {
        loadJobs();
    }

    @Override
    public void onGearPoke() {
        // TODO: 11/20/16
    }

    @Override
    public void onParseError(byte[] bytes) {
        // TODO: 11/20/16
    }
}
