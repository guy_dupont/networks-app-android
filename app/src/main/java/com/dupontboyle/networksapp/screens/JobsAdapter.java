package com.dupontboyle.networksapp.screens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.models.Job;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.ViewHolder> {
    private final PublishSubject<Job> onClickSubject = PublishSubject.create();

    private List<Job> jobs;

    JobsAdapter(List<Job> jobs) {
        this.jobs = jobs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_cell, parent, false);
        return new JobsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.configure(jobs.get(position));
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    Observable<Job> getItemClicks() {
        return onClickSubject.asObservable();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
        }

        void configure(Job job) {
            String jobName = job.getName();
            if (job.getCompletedAt() != null) {
                jobName += " - Done";
            }
            ((TextView) itemView).setText(jobName);
        }

        @Override
        public void onClick(View v) {
            onClickSubject.onNext(jobs.get(getAdapterPosition()));
        }
    }
}
