package com.dupontboyle.networksapp.screens;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    public static final int GEAR_SELECT_CLIENT_REQUEST = 2323;
    public static final int GEAR_SELECT_GEAR_CODE = 2325;

    @BindView(R.id.username_input)
    EditText usernameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                                                  new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                                                  Constants.STORAGE_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @OnClick(R.id.gear_button)
    public void gearButtonClicked() {
        if (recordUsername()) {
            moveToGearSelect(GEAR_SELECT_CLIENT_REQUEST);
        }
    }

    @OnClick(R.id.register_button)
    public void registerButtonClicked() {
        if (recordUsername()) {
            moveToGearSelect(GEAR_SELECT_GEAR_CODE);
        }
    }

    private void moveToGearSelect(int requestCode) {
        Intent intent = new Intent(this, GearSelectionActivity.class);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        Intent intent;
        switch (requestCode) {
            case GEAR_SELECT_CLIENT_REQUEST:
                intent = new Intent(HomeActivity.this, FileSelectionActivity.class);
                break;
            default:
                intent = new Intent(HomeActivity.this, GearActivity.class);
                break;
        }
        intent.putExtras(data);
        startActivity(intent);
    }

    @OnClick(R.id.jobs_button)
    public void jobsButtonClicked() {
        if (recordUsername()) {
            Intent intent = new Intent(this, JobsActivity.class);
            startActivity(intent);
        }
    }

    private boolean recordUsername() {
        if (!TextUtils.isEmpty(getUsernameInput())) {
            SharedPreferencesManager.getInstance(this).setUsername(getUsernameInput());
            return true;
        }

        Toast.makeText(this, R.string.username_prompt, Toast.LENGTH_SHORT).show();
        return false;
    }

    private String getUsernameInput() {
        return usernameEditText.getText().toString();
    }
}
