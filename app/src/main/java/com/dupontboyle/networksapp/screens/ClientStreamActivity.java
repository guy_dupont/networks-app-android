package com.dupontboyle.networksapp.screens;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.dupontboyle.networksapp.interfaces.CloudMessageReceiver;
import com.dupontboyle.networksapp.messaging.FcmReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Completable;
import rx.CompletableEmitter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import timber.log.Timber;

public class ClientStreamActivity extends AppCompatActivity implements CloudMessageInterface {
    private static final float FULL_VOL = 1f;
    private static final float NO_VOL = 0f;

    CloudMessageReceiver cloudMessageReceiver;
    Subscription streamSub;
    MediaPlayer mediaPlayer;
    AudioTrack mAudioTrack;
    boolean isStreaming;
    String originalFilePath;

    @BindView(R.id.status_text)
    TextView statusText;

    @BindView(R.id.switch_compat)
    SwitchCompat switchCompat;

    @BindView(R.id.switch_container)
    LinearLayout switchContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_stream);
        ButterKnife.bind(this);
        cloudMessageReceiver = new FcmReceiver(this);
        originalFilePath = getIntent().getStringExtra(Constants.EXTRA_FILENAME);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (mediaPlayer != null) {
                        mediaPlayer.setVolume(FULL_VOL, FULL_VOL);
                    }
                    if (mAudioTrack != null) {
                        mAudioTrack.setVolume(NO_VOL);
                    }
                } else {
                    if (mediaPlayer != null) {
                        mediaPlayer.setVolume(NO_VOL, NO_VOL);
                    }
                    if (mAudioTrack != null) {
                        mAudioTrack.setVolume(FULL_VOL);
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        cloudMessageReceiver.register(this);
        setWaitingForSchedule();
    }

    @Override
    protected void onStop() {
        super.onStop();
        cloudMessageReceiver.unregister(this);
        if (streamSub != null) {
            streamSub.unsubscribe();
        }
    }

    @Override
    public void onJobProgressUpdate(String jobId, long processed, long totalLength) {

    }

    private void setWaitingForSchedule() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switchContainer.setVisibility(View.GONE);
                setStatusText("Waiting for job to be scheduled...");
            }
        });

    }

    private void setWaitingForStream() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switchContainer.setVisibility(View.GONE);
                setStatusText("Waiting for stream to start...");
            }
        });
    }

    private void setCurrentlyStreaming() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switchContainer.setVisibility(View.VISIBLE);
                setStatusText("Streaming live audio!");
            }
        });

    }

    @Override
    public void onStreamAddress(String streamAddress) {
        setWaitingForStream();
        beginStream(streamAddress);
    }

    private void popToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ClientStreamActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setStatusText(final String message) {
        statusText.setText(message);
    }

    private void beginStream(final String address) {
        Completable streamCompletable = Completable
                .fromEmitter(new Action1<CompletableEmitter>() {
                    @Override
                    public void call(CompletableEmitter completableEmitter) {
                        try {
                            final Socket socket = new Socket(address, 9099);
                            mediaPlayer = new MediaPlayer();
                            int bufferSize = AudioTrack.getMinBufferSize(Constants.STREAM_BIT_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
                            mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, Constants.STREAM_BIT_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize, AudioTrack.MODE_STREAM);
                            completableEmitter.setSubscription(Subscriptions.create(new Action0() {
                                @Override
                                public void call() {
                                    isStreaming = false;
                                    mediaPlayer.release();
                                    mAudioTrack.release();
                                    mediaPlayer = null;
                                    mAudioTrack = null;
                                    try {
                                        socket.close();
                                    } catch (IOException e) {
                                        Timber.e(e);
                                    }

                                }
                            }));
                            mediaPlayer.setDataSource(originalFilePath);
                            mediaPlayer.prepare();
                            mediaPlayer.setVolume(NO_VOL, NO_VOL);
                            isStreaming = true;
                            mAudioTrack.play();
                            byte[] data = new byte[bufferSize];

                            InputStream inputStream = socket.getInputStream();
                            ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                            int read = inputStream.read(data);
                            setCurrentlyStreaming();
                            mediaPlayer.start();
                            while (read > 0 && isStreaming) {
                                mAudioTrack.write(byteBuffer.array(), 0, byteBuffer.array().length);
                                read = inputStream.read(data);
                            }
                            socket.close();
                            mAudioTrack.stop();
                            completableEmitter.onCompleted();

                        } catch (Exception e) {
                            completableEmitter.onError(e);
                            Timber.e(e);
                        }
                    }
                });

        // offset by 5 seconds to allow gear to prepare
        streamSub = Completable.timer(5000, TimeUnit.MILLISECONDS)
                .doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        popToast("Attempting to create stream...");
                    }
                })
                .andThen(streamCompletable)
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer integer, Throwable throwable) {
                        popToast("Error initiating stream, retrying...");
                        return integer < 4;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action0() {
                    @Override
                    public void call() {
                        popToast("Stream complete");
                        finish();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        popToast("Error initiating stream, cancelling job");
                        Timber.e(throwable);
                        finish();
                    }
                });
    }

    @Override
    public void onJobFinished(String jobId) {

    }

    @Override
    public void onGearPoke() {

    }

    @Override
    public void onParseError(byte[] bytes) {

    }
}
