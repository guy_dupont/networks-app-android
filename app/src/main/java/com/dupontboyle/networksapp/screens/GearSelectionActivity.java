package com.dupontboyle.networksapp.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.models.GearDef;
import com.dupontboyle.networksapp.net.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class GearSelectionActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView gearRecyclerView;

    private List<GearDef> gearDefs;
    private GearDefsAdapter adapter;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gear_selection);

        ButterKnife.bind(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        gearRecyclerView.setLayoutManager(layoutManager);

        gearDefs = new ArrayList<>();
        adapter = new GearDefsAdapter(gearDefs);
        gearRecyclerView.setAdapter(adapter);

        subscriptions.add(adapter.getItemClicks()
                                  .subscribe(new Action1<GearDef>() {
                                      @Override
                                      public void call(GearDef gearDef) {
                                          Intent intent = new Intent().putExtra(Constants.EXTRA_GEAR_DEF_ID, gearDef.getId());
                                          setResult(RESULT_OK, intent);
                                          finish();
                                      }
                                  }));

        loadGearDefs();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        subscriptions.clear();
    }

    private void loadGearDefs() {
        subscriptions.add(RetrofitClient.getBackendApi(this).getGearDefs()
                                  .subscribeOn(Schedulers.io())
                                  .observeOn(AndroidSchedulers.mainThread())
                                  .subscribe(new Action1<List<GearDef>>() {
                                      @Override
                                      public void call(List<GearDef> gearDefs) {
                                          updateGearDefs(gearDefs);
                                      }
                                  }, new Action1<Throwable>() {
                                      @Override
                                      public void call(Throwable throwable) {
                                          Timber.e(throwable);
                                      }
                                  }));
    }

    private void updateGearDefs(List<GearDef> gearDefs) {
        this.gearDefs.addAll(gearDefs);
        adapter.notifyDataSetChanged();
    }
}
