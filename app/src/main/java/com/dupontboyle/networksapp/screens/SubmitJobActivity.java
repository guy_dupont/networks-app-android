package com.dupontboyle.networksapp.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.dupontboyle.networksapp.net.RetrofitClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class SubmitJobActivity extends AppCompatActivity {
    @BindView(R.id.track_name_input)
    EditText trackNameEditText;

    private String gearDefId;
    private String filename;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_job);

        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        gearDefId = extras.getString(Constants.EXTRA_GEAR_DEF_ID);
        filename = extras.getString(Constants.EXTRA_FILENAME);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        subscriptions.clear();
    }

    @OnClick(R.id.submit_button)
    public void submitButtonClicked() {
        sendRequest(false);
    }

    @OnClick(R.id.stream_button)
    public void streamButtonClicked() {
        sendRequest(true);
    }

    private void sendRequest(boolean streamJob) {
        String trackName = getTrackNameInput();
        if (!TextUtils.isEmpty(trackName)) {
            uploadFile(trackName, streamJob);
        } else {
            Toast.makeText(this, R.string.track_name_prompt, Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadFile(String trackName, final boolean streamJob) {
        String username = SharedPreferencesManager.getInstance(this).getUsername();
        final File file = new File(filename);
        RequestBody body = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        subscriptions.add(RetrofitClient.getBackendApi(this)
                                  .uploadFile(gearDefId, username, FirebaseInstanceId.getInstance()
                                          .getToken(), trackName, streamJob, body)
                                  .subscribeOn(Schedulers.io())
                                  .observeOn(AndroidSchedulers.mainThread())
                                  .subscribe(new Action1<Void>() {
                                      @Override
                                      public void call(Void aVoid) {
                                          Class activityClass = streamJob ? ClientStreamActivity.class : JobsActivity.class;
                                          Intent intent = new Intent(SubmitJobActivity.this, activityClass);
                                          intent.putExtra(Constants.EXTRA_FILENAME, filename);
                                          startActivity(intent);
                                      }
                                  }, new Action1<Throwable>() {
                                      @Override
                                      public void call(Throwable throwable) {
                                          Timber.e(throwable);
                                      }
                                  }));
    }

    private String getTrackNameInput() {
        return trackNameEditText.getText().toString();
    }
}
