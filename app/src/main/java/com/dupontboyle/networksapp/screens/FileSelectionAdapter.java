package com.dupontboyle.networksapp.screens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dupontboyle.networksapp.R;

import java.io.File;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

class FileSelectionAdapter extends RecyclerView.Adapter<FileSelectionAdapter.ViewHolder> {
    private final PublishSubject<File> onClickSubject = PublishSubject.create();

    private List<File> files;

    FileSelectionAdapter(List<File> files) {
        this.files = files;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filename_cell, parent, false);
        return new FileSelectionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.configure(files.get(position));
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    Observable<File> getItemClicks() {
        return onClickSubject.asObservable();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
        }

        void configure(File file) {
            ((TextView) itemView).setText(file.getName());
        }

        @Override
        public void onClick(View v) {
            onClickSubject.onNext(files.get(getAdapterPosition()));
        }
    }
}
