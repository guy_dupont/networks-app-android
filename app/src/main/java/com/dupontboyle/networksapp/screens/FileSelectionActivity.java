package com.dupontboyle.networksapp.screens;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class FileSelectionActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView filesRecyclerView;

    private List<File> files;
    private FileSelectionAdapter adapter;
    private String gearDefId;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_selection);

        ButterKnife.bind(this);

        gearDefId = getIntent().getStringExtra(Constants.EXTRA_GEAR_DEF_ID);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        filesRecyclerView.setLayoutManager(layoutManager);

        files = new ArrayList<>();
        adapter = new FileSelectionAdapter(files);
        filesRecyclerView.setAdapter(adapter);

        subscriptions.add(adapter.getItemClicks()
                .subscribe(new Action1<File>() {
                    @Override
                    public void call(File file) {
                        Intent intent = new Intent(FileSelectionActivity.this, SubmitJobActivity.class);
                        intent.putExtra(Constants.EXTRA_GEAR_DEF_ID, gearDefId);
                        intent.putExtra(Constants.EXTRA_FILENAME, file.getAbsolutePath());
                        startActivity(intent);
                    }
                }));

        showFiles();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        subscriptions.clear();
    }

    private void showFiles() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        File[] files = dir.listFiles();
        Collections.addAll(this.files, files);
        adapter.notifyDataSetChanged();
    }
}
