package com.dupontboyle.networksapp.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.dupontboyle.networksapp.net.RetrofitClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RequestHostActivity extends AppCompatActivity {
    @BindView(R.id.host_input)
    EditText hostEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_host);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.continue_button)
    public void continueClicked() {
        String url = "http://" + hostEditText.getText().toString() + ":5000/";
        SharedPreferencesManager.getInstance(this).setServerUrl(url);
        startActivity(new Intent(this, HomeActivity.class));
    }

    @OnClick(R.id.use_heroku_button)
    public void useHerokuClicked() {
        SharedPreferencesManager.getInstance(this).setServerUrl(RetrofitClient.HEROKU_ENDPOINT);
        startActivity(new Intent(this, HomeActivity.class));
    }
}
