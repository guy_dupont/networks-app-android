package com.dupontboyle.networksapp.screens;

import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.dupontboyle.networksapp.Constants;
import com.dupontboyle.networksapp.R;
import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.dupontboyle.networksapp.interfaces.CloudMessageReceiver;
import com.dupontboyle.networksapp.messaging.FcmReceiver;
import com.dupontboyle.networksapp.models.GearInstance;
import com.dupontboyle.networksapp.net.RetrofitClient;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.AsyncEmitter;
import rx.Observable;
import rx.Single;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import timber.log.Timber;

public class GearActivity extends AppCompatActivity implements CloudMessageInterface {

    CloudMessageReceiver cloudMessageReceiver;
    SharedPreferencesManager sharedPreferencesManager;
    MediaPlayer mediaPlayer;
    Subscription gearSub;
    ServerSocket serverSocket;
    Socket socket;
    boolean processingFile;
    boolean isStreaming;

    @BindView(R.id.status_text)
    TextView statusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gear);
        ButterKnife.bind(this);
        cloudMessageReceiver = new FcmReceiver(this);
        sharedPreferencesManager = SharedPreferencesManager.getInstance(this);
        Intent intent = getIntent();
        String gearDefId = intent.getStringExtra(Constants.EXTRA_GEAR_DEF_ID);
        String studioId = sharedPreferencesManager.getUsername();
        String gearId = sharedPreferencesManager.getGearId();
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        registerWithServer(gearDefId, studioId, fcmToken, gearId, null);
        mediaPlayer = new MediaPlayer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        cloudMessageReceiver.register(this);
        queryForNextJob();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (gearSub != null) {
            gearSub.unsubscribe();
        }
        cloudMessageReceiver.unregister(this);
    }

    private void registerWithServer(String gearDefId, String studioId, String fcmToken, String gearId, String name) {
        RetrofitClient.getBackendApi(this).registerAsGear(fcmToken, studioId, gearDefId, gearId, name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GearInstance>() {
                    @Override
                    public void call(GearInstance gearInstance) {
                        onGearInfoReceived(gearInstance);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Timber.e(throwable);
                        Toast.makeText(GearActivity.this, "Unable to register.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }

    private Single<JobInProgress> writeFileToStorage(final ResponseBody body, final JobInProgress job) {
        return Single.fromCallable(new Callable<JobInProgress>() {
            @Override
            public JobInProgress call() throws Exception {
                InputStream inputStream = body.byteStream();
                FileOutputStream fileOutputStream = new FileOutputStream(job.getOriginalFilePath());
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, bytesRead);
                }
                inputStream.close();
                fileOutputStream.close();
                Timber.d("Wrote file");
                return job;
            }
        });
    }

    private void onGearInfoReceived(GearInstance gearInstance) {
        sharedPreferencesManager.setGearId(gearInstance.getId());
        Toast.makeText(this, "Gear up and running. Streaming mode: " + gearInstance.getStreamingJob(), Toast.LENGTH_LONG).show();
    }

    private static class JobInProgress {
        private final String jobId;
        private String originalFilePath;
        private String processedFilePath;
        private String streamAddress;

        public JobInProgress(String jobId, String streamAddress) {
            this.jobId = jobId;

            this.originalFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
                    .getAbsolutePath() + "/" +
                    jobId;
            this.processedFilePath = this.originalFilePath + "_.wav";
            this.streamAddress = streamAddress;
        }


        public String getJobId() {
            return jobId;
        }

        public String getOriginalFilePath() {
            return originalFilePath;
        }

        public String getProcessedFilePath() {
            return processedFilePath;
        }

        public boolean shouldStream() {
            return !TextUtils.isEmpty(streamAddress);
        }
    }

    private String jobIdFromHeaders(Response<ResponseBody> response) {
        String header = response.headers().get("Content-Disposition");
        return header.split("\"")[1];
    }

    private String streamAddresFromHeaders(Response<ResponseBody> response) {
        return response.headers().get("StreamAddress");
    }

    private void setIdle() {
        setStatusText("Waiting for next Job to process.");
    }

    private void setDownloadingJob() {
        setStatusText("Checking for next job...");
    }

    private void setDownloadedJob(String jobId) {
        setStatusText("Succesfully downloaded job: " + jobId);
    }

    private void setWaitingForStream(String jobId) {
        setStatusText("Waiting for stream to start...");
    }

    private void setCurrentlyStreaming(String jobId) {
        setStatusText("Currently streaming job: " + jobId);
    }

    private void setCurrentlyProcessing(String jobId) {
        setStatusText("Currently processing job: " + jobId);
    }

    private void setStatusText(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                statusText.setText(message);
            }
        });
    }

    private void popToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(GearActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static class EmptyBodyException extends Throwable {

    }

    private void queryForNextJob() {
        String gearId = sharedPreferencesManager.getGearId();
        if (TextUtils.isEmpty(gearId) || processingFile) {
            return;
        }
        processingFile = true;
        gearSub = RetrofitClient.getBackendApi(this).getNextJobForGear(gearId)
                .toSingle()
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Response<ResponseBody>, Single<JobInProgress>>() {
                    @Override
                    public Single<JobInProgress> call(Response<ResponseBody> responseBodyResponse) {
                        setDownloadingJob();
                        if (responseBodyResponse.isSuccessful() && responseBodyResponse.body() != null) {
                            String jobId = jobIdFromHeaders(responseBodyResponse);
                            String streamAddress = streamAddresFromHeaders(responseBodyResponse);
                            Timber.d(jobId);
                            JobInProgress job = new JobInProgress(jobId, streamAddress);
                            setDownloadedJob(jobId);
                            return writeFileToStorage(responseBodyResponse.body(), job);
                        }
                        setIdle();
                        return Single.error(new EmptyBodyException());
                    }
                })
                .flatMap(new Func1<JobInProgress, Single<JobInProgress>>() {
                    @Override
                    public Single<JobInProgress> call(JobInProgress job) {
                        return !job.shouldStream() ? fullProcessObservable(job) : streamObservable(job);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<JobInProgress>() {
                    @Override
                    public void call(JobInProgress s) {
                        Toast.makeText(GearActivity.this, "Done processing.", Toast.LENGTH_SHORT).show();
                        processingFile = false;
                        queryForNextJob();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        processingFile = false;
                        if (!(throwable instanceof EmptyBodyException)) {
                            queryForNextJob();
                        } else {
                            setIdle();
                        }
                    }
                });
    }

    private Single<JobInProgress> streamObservable(final JobInProgress jobInProgress) {
        return Observable
                .fromEmitter(new Action1<AsyncEmitter<JobInProgress>>() {
                    @Override
                    public void call(final AsyncEmitter<JobInProgress> asyncEmitter) {
                        int bufferLength = 0;
                        int bufferSize;
                        byte[] audioData;
                        int bufferReadResult;
                        OutputStream outputStream;
                        AudioRecord audioRecord;
                        mediaPlayer = new MediaPlayer();
                        try {
                            setWaitingForStream(jobInProgress.getJobId());
                            serverSocket = new ServerSocket(Constants.STREAM_PORT);
                            serverSocket.setSoTimeout(30000);
                            asyncEmitter.setSubscription(Subscriptions.create(new Action0() {
                                @Override
                                public void call() {
                                    isStreaming = false;
                                    mediaPlayer.release();
                                    try {
                                        serverSocket.close();
                                        if (socket != null) {
                                            socket.close();
                                        }
                                    } catch (Exception e) {
                                        Timber.e(e);
                                    }
                                }
                            }));
                            socket = serverSocket.accept();


                            bufferSize = AudioRecord.getMinBufferSize(Constants.STREAM_BIT_RATE,
                                                                      AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_DEFAULT);
                            outputStream = socket.getOutputStream();

                            if (bufferSize <= 2048) {
                                bufferLength = 2048;
                            } else if (bufferSize <= 4096) {
                                bufferLength = 4096;
                            }

                            audioRecord = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, Constants.STREAM_BIT_RATE,
                                                          AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_DEFAULT, bufferLength);
                            audioData = new byte[bufferLength];
                            audioRecord.startRecording();

                            mediaPlayer.setDataSource(jobInProgress.getOriginalFilePath());
                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    Timber.d("Played and recorded file");
                                    isStreaming = false;
                                    mediaPlayer.release();
                                }
                            });
                            isStreaming = true;
                            mediaPlayer.prepare();
                            mediaPlayer.start();

                            setCurrentlyStreaming(jobInProgress.getJobId());
                            while (isStreaming) {
                                bufferReadResult = audioRecord.read(audioData, 0, audioData.length);
                                outputStream.write(audioData, 0, bufferReadResult);
                            }
                            audioRecord.stop();
                            audioRecord.release();
                            asyncEmitter.onNext(jobInProgress);
                            asyncEmitter.onCompleted();
                        } catch (IOException e) {
                            asyncEmitter.onError(e);
                        }

                    }
                }, AsyncEmitter.BackpressureMode.NONE)
                .onErrorReturn(new Func1<Throwable, JobInProgress>() {
                    @Override
                    public JobInProgress call(Throwable throwable) {
                        popToast("Error creating stream, marking as complete.");
                        Timber.e(throwable);
                        return jobInProgress;
                    }
                })
                .flatMap(new Func1<JobInProgress, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(JobInProgress jobInProgress) {
                        return RetrofitClient.getBackendApi(GearActivity.this).markStreamAsCompleted(jobInProgress.getJobId());
                    }
                }, new Func2<JobInProgress, Void, JobInProgress>() {
                    @Override
                    public JobInProgress call(JobInProgress jobInProgress, Void aVoid) {
                        return jobInProgress;
                    }
                })
                .toSingle();

    }

    private Single<JobInProgress> fullProcessObservable(JobInProgress jobInProgress) {
        setCurrentlyProcessing(jobInProgress.getJobId());
        return processFile(jobInProgress)
                .flatMap(new Func1<JobInProgress, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(JobInProgress jobInProgress) {
                        File file = new File(jobInProgress.getProcessedFilePath());
                        RequestBody body = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                        return RetrofitClient.getBackendApi(GearActivity.this)
                                .uploadCompletedFile(jobInProgress.getJobId(), body)
                                .subscribeOn(Schedulers.io());
                    }
                }, new Func2<JobInProgress, Void, JobInProgress>() {
                    @Override
                    public JobInProgress call(JobInProgress jobInProgress, Void aVoid) {
                        return jobInProgress;
                    }
                })
                .toSingle();
    }

    private Observable<JobInProgress> processFile(final JobInProgress job) {
        return Observable.fromEmitter(new Action1<AsyncEmitter<JobInProgress>>() {
            @Override
            public void call(final AsyncEmitter<JobInProgress> asyncEmitter) {
                try {
                    Timber.d("attempting to play file");
                    final MediaRecorder mediaRecorder = new MediaRecorder();
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
                    mediaRecorder.setAudioChannels(1);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
                    mediaRecorder.setOutputFile(job.getProcessedFilePath());
                    mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                    final MediaPlayer mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(job.getOriginalFilePath());
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Timber.d("Played and recorded file");
                            mediaRecorder.stop();
                            mediaRecorder.release();
                            mediaPlayer.release();
                            asyncEmitter.onNext(job);
                            asyncEmitter.onCompleted();
                        }
                    });
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                    mediaPlayer.prepare();
                    mediaPlayer.start();

                } catch (IOException e) {
                    asyncEmitter.onError(e);
                }

            }
        }, AsyncEmitter.BackpressureMode.NONE);
    }

    @Override
    public void onJobProgressUpdate(String jobId, long processed, long totalLength) {

    }

    @Override
    public void onStreamAddress(String streamAddress) {

    }

    @Override
    public void onJobFinished(String jobId) {

    }

    @Override
    public void onGearPoke() {
        queryForNextJob();
    }

    @Override
    public void onParseError(byte[] bytes) {

    }
}
