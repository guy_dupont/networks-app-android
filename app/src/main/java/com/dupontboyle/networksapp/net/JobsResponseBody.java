package com.dupontboyle.networksapp.net;

import com.dupontboyle.networksapp.models.Job;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobsResponseBody {
    @SerializedName("jobs")
    private List<Job> jobs;

    public List<Job> getJobs() {
        return jobs;
    }
}
