package com.dupontboyle.networksapp.net;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import com.dupontboyle.networksapp.NetworksApplication;
import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class RetrofitClient {
    private static final int CONNECTION_TIMEOUT = 30;
    public static final String HEROKU_ENDPOINT = "http://young-retreat-95370.herokuapp.com/";

    private static BackendApi instance;

    public static BackendApi getBackendApi(Context context) {
        if (instance == null) {
            instance = createRestApi(context);
        }
        return instance;
    }

    private static BackendApi createRestApi(Context context) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        String address = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
                        Request request = chain.request().newBuilder()
                                .addHeader("X-Forwarded-For", address).build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Timber.v(message);
                    }
                }).setLevel(HttpLoggingInterceptor.Level.BASIC))
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .build();

        String url = SharedPreferencesManager.getInstance(NetworksApplication.context).getServerUrl();
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient)
                .addConverterFactory(getConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(BackendApi.class);
    }

    private static Converter.Factory getConverterFactory() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return GsonConverterFactory.create(gson);
    }

    private RetrofitClient() {

    }
}
