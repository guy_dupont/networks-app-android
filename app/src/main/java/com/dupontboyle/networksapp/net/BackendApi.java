package com.dupontboyle.networksapp.net;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.dupontboyle.networksapp.models.GearDef;
import com.dupontboyle.networksapp.models.GearInstance;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import rx.Observable;

public interface BackendApi {
    @GET("/gearDef")
    Observable<List<GearDef>> getGearDefs();

    @Multipart
    @POST("/upload")
    Observable<Void> uploadFile(@Query("gear_def_id") String gearDefId,
                                @Query("user_id") String userId,
                                @Query("fcm_token") String fcmToken,
                                @Query("name") String trackName,
                                @Query("stream") boolean streamJob,
                                @Part("audio") RequestBody body);

    @Multipart
    @POST("/uploadCompleted/{job_id}")
    Observable<Void> uploadCompletedFile(@Path("job_id") String jobId,
                                         @Part("audio") RequestBody body);

    @POST("/streamJobCompleted/{job_id}")
    Observable<Void> markStreamAsCompleted(@Path("job_id") String jobId);

    @GET("/jobsForUser/{user_id}")
    Observable<JobsResponseBody> getJobs(@Path("user_id") String userId);

    @POST("/updateUserToken/{user_id}")
    Observable<Void> updateUserToken(@Path("user_id") String userId,
                                     @Query("fcm_token") String fcmToken);

    @POST("/updateGearToken/{gear_id}")
    Observable<Void> updateGearToken(@Path("gear_id") String gearId,
                                     @Query("fcm_token") String fcmToken);

    @Streaming
    @GET("/completedFile/{job_id}")
    Observable<Response<ResponseBody>> downloadFile(@Path("job_id") String jobId);

    @POST("/register")
    Observable<GearInstance> registerAsGear(@NonNull @Query("fcm_token") String fcmToken,
                                            @NonNull @Query("studio_id") String studioId,
                                            @NonNull @Query("gear_def_id") String gearDefId,
                                            @Nullable @Query("id") String id,
                                            @Nullable @Query("name") String name);

    @Streaming
    @GET("/nextJob/{gear_id}")
    Observable<Response<ResponseBody>> getNextJobForGear(@Path("gear_id") String gearId);
}
