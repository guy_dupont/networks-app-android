package com.dupontboyle.networksapp.messaging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.dupontboyle.networksapp.interfaces.CloudMessageReceiver;
import com.dupontboyle.networksapp.models.udp.UdpSpec;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import rx.AsyncEmitter;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class FcmReceiver implements CloudMessageReceiver {
    public static final String CONTENT_FILTER = "db-fcm-receiver";
    public static final String PAYLOAD_KEY = "payload_key";
    private HashMap<CloudMessageInterface, Subscription> subscriptionHashMap = new HashMap<>();
    private JsonParser jsonParser = new JsonParser();
    private Observable<JsonObject> jsonObservable;

    public FcmReceiver(Context context) {
        final LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        jsonObservable = Observable.fromEmitter(new Action1<AsyncEmitter<JsonObject>>() {
            @Override
            public void call(final AsyncEmitter<JsonObject> jsonObjectAsyncEmitter) {
                final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String payload = intent.getStringExtra(PAYLOAD_KEY);
                        parseString(payload, jsonObjectAsyncEmitter);
                    }
                };
                localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(CONTENT_FILTER));
                jsonObjectAsyncEmitter.setSubscription(Subscriptions.create(new Action0() {
                    @Override
                    public void call() {
                        localBroadcastManager.unregisterReceiver(broadcastReceiver);
                    }
                }));
            }
        }, AsyncEmitter.BackpressureMode.NONE).subscribeOn(Schedulers.newThread()).share();
    }

    private void parseString(String payload, AsyncEmitter<JsonObject> emitter) {
        try {
            JsonObject jsonObject = jsonParser.parse(payload).getAsJsonObject();
            emitter.onNext(jsonObject);
        } catch (Exception e) {
            emitter.onError(e);
        }
    }

    @Override
    public void register(final CloudMessageInterface cloudMessageInterface) {
        Subscription subscription = jsonObservable.subscribe(new Action1<JsonObject>() {
            @Override
            public void call(JsonObject jsonObject) {
                UdpSpec.parseFromFcm(jsonObject, cloudMessageInterface);
            }
        });
        subscriptionHashMap.put(cloudMessageInterface, subscription);
    }

    @Override
    public void unregister(CloudMessageInterface cloudMessageInterface) {
        subscriptionHashMap.remove(cloudMessageInterface).unsubscribe();
    }
}
