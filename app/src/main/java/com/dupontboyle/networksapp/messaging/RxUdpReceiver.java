package com.dupontboyle.networksapp.messaging;

import com.dupontboyle.networksapp.interfaces.CloudMessageInterface;
import com.dupontboyle.networksapp.interfaces.CloudMessageReceiver;
import com.dupontboyle.networksapp.models.udp.UdpSpec;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

import rx.AsyncEmitter;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import timber.log.Timber;

public class RxUdpReceiver implements CloudMessageReceiver {
    public static final int DEFAULT_BUFFER_SIZE = 1024;
    public static final int UDP_LISTEN_PORT = 9099;
    private HashMap<CloudMessageInterface, Subscription> subscriptionHashMap = new HashMap<>();
    private Observable<DatagramPacket> sharedObservable;
    private DatagramPacket datagramPacket;
    private DatagramSocket socket;

    public RxUdpReceiver(final int port) {
        this(DEFAULT_BUFFER_SIZE, port);
    }

    public RxUdpReceiver(int bufferLength, final int port) {
        byte[] buffer = new byte[bufferLength];
        datagramPacket = new DatagramPacket(buffer, bufferLength);
        sharedObservable = Observable.fromEmitter(new Action1<AsyncEmitter<DatagramPacket>>() {
            @Override
            public void call(final AsyncEmitter<DatagramPacket> longAsyncEmitter) {
                try {
                    socket = new DatagramSocket(port);
                } catch (SocketException e) {
                    Timber.e(e);
                }
                longAsyncEmitter.setSubscription(Subscriptions.create(new Action0() {
                    @Override
                    public void call() {
                        socket.close();
                    }
                }));

                try {
                    while (!socket.isClosed()) {
                        socket.receive(datagramPacket);
                        longAsyncEmitter.onNext(datagramPacket);
                    }
                } catch (IOException e) {
                    // if we get a socket exception, just means the socekt was closed while hanging on receive.  expected.
                    if (!(e instanceof SocketException)) {
                        longAsyncEmitter.onError(e);
                    }
                }

            }
        }, AsyncEmitter.BackpressureMode.BUFFER).subscribeOn(Schedulers.newThread()).share();

    }

    public void register(final CloudMessageInterface cloudMessageInterface) {
        Subscription subscription = sharedObservable.subscribe(new Action1<DatagramPacket>() {
            @Override
            public void call(DatagramPacket datagramPacket) {
                UdpSpec.parseFromUdp(datagramPacket, cloudMessageInterface);
            }
        });
        subscriptionHashMap.put(cloudMessageInterface, subscription);
    }

    public void unregister(CloudMessageInterface cloudMessageInterface) {
        subscriptionHashMap.remove(cloudMessageInterface).unsubscribe();
    }

}
