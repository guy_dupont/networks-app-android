package com.dupontboyle.networksapp.services;

import android.text.TextUtils;
import android.widget.Toast;

import com.dupontboyle.networksapp.SharedPreferencesManager;
import com.dupontboyle.networksapp.net.BackendApi;
import com.dupontboyle.networksapp.net.RetrofitClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import rx.Observable;
import rx.functions.Action1;
import timber.log.Timber;

public class FirebaseIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SharedPreferencesManager sharedPreferencesManager = SharedPreferencesManager.getInstance(this);
        String gearId = sharedPreferencesManager.getGearId();
        BackendApi backendApi = RetrofitClient.getBackendApi(this);
        Observable<Void> requestObservable;
        if (!TextUtils.isEmpty(gearId)) {
            requestObservable = backendApi.updateGearToken(gearId, refreshedToken);
        } else {
            String clientID = sharedPreferencesManager.getUsername();
            if (TextUtils.isEmpty(clientID)) {
                return;
            }
            requestObservable = backendApi.updateUserToken(clientID, refreshedToken);
        }

        requestObservable.subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                Timber.d("Successfully updated token on server.");
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                String msg =  "Error refreshing FCM token.";
                Timber.e(throwable);
                Toast.makeText(FirebaseIdService.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
