package com.dupontboyle.networksapp.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.dupontboyle.networksapp.messaging.FcmReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import timber.log.Timber;

public class FcmService extends FirebaseMessagingService {

    private final static String TAG = "FcmService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            String jsonPayload = remoteMessage.getData().get("obj");
            Timber.d(jsonPayload);
            Intent intent = new Intent(FcmReceiver.CONTENT_FILTER);
            intent.putExtra(FcmReceiver.PAYLOAD_KEY, jsonPayload);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }
}
