package com.dupontboyle.networksapp;

public class Constants {
    public static final String EXTRA_GEAR_DEF_ID = "gearDefId";
    public static final String EXTRA_FILENAME = "filename";

    public static final int STORAGE_PERMISSION_REQUEST_CODE = 100;
    public static final int STREAM_PORT = 9099;
    public static final int STREAM_BIT_RATE = 22050;
}
