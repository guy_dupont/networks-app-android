package com.dupontboyle.networksapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesManager {
    private static final String KEY_USERNAME = "username";
    private static final String KEY_GEAR_ID = "gearID";
    private static final String KEY_SERVER_URL = "serverIp";

    private static SharedPreferencesManager instance;

    public static SharedPreferencesManager getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPreferencesManager(context);
        }
        return instance;
    }

    private final SharedPreferences preferences;

    private SharedPreferencesManager(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getUsername() {
        return preferences.getString(KEY_USERNAME, "");
    }

    public void setUsername(String username) {
        preferences.edit().putString(KEY_USERNAME, username).apply();
    }

    public String getGearId() {
        return preferences.getString(KEY_GEAR_ID, "");
    }

    public void setGearId(String gearId) {
        preferences.edit().putString(KEY_GEAR_ID, gearId).apply();
    }

    public String getServerUrl() {
        return preferences.getString(KEY_SERVER_URL, "");
    }

    public void setServerUrl(String url) {
        preferences.edit().putString(KEY_SERVER_URL, url).apply();
    }
}
